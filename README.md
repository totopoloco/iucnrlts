# iucnRLTS

API IUCN Red List of Threatened Species

# Java 11
***This project uses Java 11 to compile and run***

# Clone project and launch server

## Clone
`$ git clone git@gitlab.com:totopoloco/iucnrlts.git`

## Launch server
`$ ./gradlew bootRun`

# Usage

## Load all regions
http://localhost:8080/loadListAvailableRegions

## Get a random region
http://localhost:8080/getRandomRegion

## Load the list of all species within a random region
http://localhost:8080/listSpeciesInRandomRegion

## Species in a region
http://localhost:8080/listSpeciesInRegion

## Filter the results for Critically Endangered species
http://localhost:8080/listSpecieseInRegion

## Fetch conservation measures
http://localhost:8080/fetchConservationMeasures

## Filter species by class
http://localhost:8080/listSpeciesByClassInRegion