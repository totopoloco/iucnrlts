package at.mavila.iucnrlts.controllers;

import at.mavila.iucnrlts.ConnectionUtils;
import at.mavila.iucnrlts.pojos.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;

@RestController
@Component
public class SpeciesController {

    private static final Logger LOGGER = LoggerFactory.getLogger(SpeciesController.class);
    @Value("${iucn.token}")
    private String token;
    @Value("${iucn.url}")
    private String url;
    @Autowired
    private LoadRegionsForSpeciesController loadRegionsForSpeciesController;

    /**
     * Load the list of all species in the random region.
     *
     * @param pagenum optional page number, default to first page.
     * @param regionid optional, if omitted then a random region is chosen.
     * @param forceReload to reload the regions (as this is time consuming)
     * @return a SpeciesPerRegion representation
     */
    @RequestMapping(value = "/listSpeciesInRegion", method = RequestMethod.GET)
    public SpeciesPerRegion listAllSpeciesWithinRegion(
            @RequestParam(value = "pagenum", required = false) String pagenum,
            @RequestParam(value = "regionid", required = false) String regionid,
            @RequestParam(value = "forceReload", required = false) String forceReload

    ) {


        final RestTemplate restTemplate = ConnectionUtils.getRestTemplate();

        final String regionIdentifier = getRegionIdentifier(regionid, forceReload);
        final SpeciesPerRegion speciesPerRegion = restTemplate.exchange(
                this.url + "/species/region/" + regionIdentifier + "/page/" + getPage(pagenum) + "?token=" + token,
                HttpMethod.GET,
                new HttpEntity<>(new SpeciesPerRegion()),
                SpeciesPerRegion.class)
                .getBody();

        if(nonNull(speciesPerRegion)) {
            final List<Specie> result = speciesPerRegion.getResult();
            result.forEach(makeTitle(regionIdentifier));
        }

        return speciesPerRegion;

    }

    /**
     * Load the list of all species by class in the region.
     *
     * @param pagenum optional. Page number to fetch data, default to 1
     * @param regionid optional. Default to a random region
     * @param className optional. Default to MAMMAL
     * @param forceReload optional. To reload the regions (as this is time consuming)
     * @return a SpeciesPerRegion representation
     */
    @RequestMapping(value = "/listSpeciesByClassInRegion", method = RequestMethod.GET)
    public SpeciesPerRegion listSpeciesByClassInRegion(
            @RequestParam(value = "pagenum", required = false) String pagenum,
            @RequestParam(value = "regionid", required = false) String regionid,
            @RequestParam(value = "class", required = false) String className,
            @RequestParam(value = "forceReload", required = false) String forceReload

    ) {


        final SpeciesPerRegion speciesPerRegion = this.listAllSpeciesWithinRegion(pagenum, regionid, forceReload);

        speciesPerRegion.setResult(

                speciesPerRegion.getResult()
                        .stream()
                        .filter(specie -> specie.getClassName().equals(isNull(className) ? "MAMMAL" : className))
                        .collect(Collectors.toList())

        );


        return speciesPerRegion;

    }

    private Consumer<Specie> makeTitle(final String regionId) {
        return specie -> {
            final String title =
                    "Region id: <"+regionId+"> "
                    +"TaxonId: <" + specie.getTaxonid()
                    + "> Kingdom name: <" + specie.getKingdomName()
                    + "> Phylum name: <" + specie.getPhylumName()
                            + "> Class name: <" + specie.getClassName()
                            + "> Order name: <" + specie.getOrderName()
                            + "> Family name: <" + specie.getFamilyName()
                            +"> Genus name: <"+specie.getGenusName()
                            +"> Scientific name: <"+specie.getScientificName()
                            +"> Category: <"+specie.getCategory()+">"
                    ;
            specie.setTitle(title);
        };
    }


    /**
     * Lists all the species in the region.
     *
     * @param pagenum optional. Page number to fetch data, default to 1
     * @param regionid optional. Default to a random region
     * @param category optional. Default to CR. Critically endangered.
     * @param forceReload optional. To reload the regions (as this is time consuming)
     *
     * @return a SpeciesPerRegion representation
     */
    @RequestMapping(value = "/listSpecieseInRegion", method = RequestMethod.GET)
    public SpeciesPerRegion listAllSpeciesInCategoryWithinRegion(
            @RequestParam(value = "pagenum", required = false) String pagenum,
            @RequestParam(value = "regionid", required = false) String regionid,
            @RequestParam(value = "category", required = false) String category,
            @RequestParam(value = "forceReload", required = false) String forceReload

    ) {
        final SpeciesPerRegion speciesPerRegion = this.listAllSpeciesWithinRegion(pagenum, regionid, forceReload);

        speciesPerRegion.setResult(

                speciesPerRegion.getResult()
                .stream()
                .filter(specie -> specie.getCategory().equals(isNull(category) ? "CR" : category))
                .collect(Collectors.toList())
        );

        return speciesPerRegion;

    }

    /**
     * Fetches the conservation measures for he selected region and category.
     *
     * @param pagenum optional. Page number to fetch data, default to 1
     * @param regionid optional. Default to a random region
     * @param category optional. Default to CR. Critically endangered.
     * @param forceReload optional. To reload the regions (as this is time consuming)
     *
     * @return a List of the titles with the conservation measures added to the original title.
     */
    @RequestMapping(value = "/fetchConservationMeasures", method = RequestMethod.GET)
    public List<String> fetchConservationMeasures(@RequestParam(value = "pagenum", required = false) String pagenum,
                                                  @RequestParam(value = "regionid", required = false) String regionid,
                                                  @RequestParam(value = "category", required = false) String category,
                                                  @RequestParam(value = "forceReload", required = false) String forceReload
    ){

        final SpeciesPerRegion speciesPerRegion = this.listAllSpeciesInCategoryWithinRegion(pagenum, regionid, category, forceReload);

        final List<Specie> species = speciesPerRegion.getResult();

        species.forEach(specie -> {

            final String name = specie.getScientificName(); //Uses the scientific name?
            final String urlToFetch = this.url+"/measures/species/name/"+name+"?token="+this.token;
            final RestTemplate restTemplate = ConnectionUtils.getRestTemplate();

            //fetch the conservation measures
            final Measures measures = restTemplate.exchange(urlToFetch,
                    HttpMethod.GET,
                    new HttpEntity<>(new Measures()),
                    Measures.class)
                    .getBody();

            if(nonNull(measures)) {
                final List<Measure> result = measures.getResult();
                if (isNotEmpty(result)) {
                    result.forEach(measure -> {
                        final String newTitle = specie.getTitle()
                                + " Code: <" + measure.getCode() + "> " + "Title: <" + measure.getTitle() + ">";
                        specie.setTitle(newTitle);
                    });
                }
            }


        });

        return species
                .stream()
                .map(Specie::getTitle)
                .collect(Collectors.toList());

    }



    private String getRegionIdentifier(@RequestParam(value = "regionid", required = false) String regionid,
                                       @RequestParam(value = "forceReload", required = false) String forceReload

                                       ) {
        if(isNull(regionid)) {
            final Region randomRegion = this.loadRegionsForSpeciesController.getRandomRegion(forceReload);
            if(LOGGER.isDebugEnabled()){
                LOGGER.debug("Region: {}", randomRegion);
            }
            return randomRegion.getIdentifier();
        }
        return regionid;
    }

    private int getPage(final String pageNum) {
        try {
            return Integer.parseInt(pageNum);
        } catch (NumberFormatException e) {
            return 1;
        }
    }
}
