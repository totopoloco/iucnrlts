package at.mavila.iucnrlts.controllers;

import at.mavila.iucnrlts.ConnectionUtils;
import at.mavila.iucnrlts.pojos.Region;
import at.mavila.iucnrlts.pojos.Regions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static org.apache.commons.collections4.CollectionUtils.isEmpty;

@RestController
@Component
public class LoadRegionsForSpeciesController {

    @Value("${iucn.token}")
    private String token;

    @Value("${iucn.url}")
    private String url;

    private Regions currentRegions;

    private static final Logger LOGGER = LoggerFactory.getLogger(LoadRegionsForSpeciesController.class);

    /**
     * Loads all regions
     * @param forceReload optional. To reload the regions (as this is time consuming)
     * @return a Regions object
     */
    @RequestMapping(value = "/loadListAvailableRegions", method = RequestMethod.GET)
    public Regions loadList(

            @RequestParam(value = "forceReload", required = false) String forceReload

    ) {

        final RestTemplate restTemplate = ConnectionUtils.getRestTemplate();

        if(nonNull(forceReload)){
            loadCurrentRegions(restTemplate);
            return this.currentRegions;

        }

        if(isNull(this.currentRegions)){
            loadCurrentRegions(restTemplate);
            return this.currentRegions;
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("end: {}", this.currentRegions);
        }
        return this.currentRegions;

    }

    private void loadCurrentRegions(RestTemplate restTemplate) {
        this.currentRegions = restTemplate.exchange(
                this.url + "/region/list?token=" + token,
                HttpMethod.GET,
                new HttpEntity<>(new Regions()),
                Regions.class)
                .getBody();
    }

    /**
     * Gets a random region
     * @param forceReload optional. To reload the regions (as this is time consuming)
     * @return a Region object which represents en element from the list
     */
    @RequestMapping(value = "/getRandomRegion", method = RequestMethod.GET)
    public Region getRandomRegion(


            @RequestParam(value = "forceReload", required = false) String forceReload

    ) {
        final Regions regions = (isNull(this.currentRegions) || nonNull(forceReload)) ? this.loadList(forceReload) : this.currentRegions;

        if (isNull(regions) || regions.getCount() == 0) {
            return new Region();
        }

        final List<Region> regionList = regions.getResults();

        if (isEmpty(regionList)) {
            return new Region();
        }

        int regionListSize = regionList.size();
        return regionList.get(ThreadLocalRandom.current().nextInt(regionListSize) % regionListSize);
    }


}
