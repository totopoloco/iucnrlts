package at.mavila.iucnrlts;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class IucnRltsApplication extends SpringBootServletInitializer {

    private static final Logger LOGGER = LoggerFactory.getLogger(IucnRltsApplication.class);

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        final SpringApplicationBuilder sources = builder.sources(IucnRltsApplication.class);
        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("Returning: {}", sources.application().getClassLoader().getName());
        }
        return sources;
    }

    public static void main(String[] args) {
        SpringApplication.run(IucnRltsApplication.class, args);
    }

}
