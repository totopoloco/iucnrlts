package at.mavila.iucnrlts.pojos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "taxonid",
        "kingdom_name",
        "phylum_name",
        "class_name",
        "order_name",
        "family_name",
        "genus_name",
        "scientific_name",
        "infra_rank",
        "infra_name",
        "population",
        "category","title"
})
public class Specie {
    @JsonProperty("taxonid")
    private Long taxonid;
    @JsonProperty("kingdom_name")
    private String kingdomName;
    @JsonProperty("phylum_name")
    private String phylumName;
    @JsonProperty("class_name")
    private String className;
    @JsonProperty("order_name")
    private String orderName;
    @JsonProperty("family_name")
    private String familyName;
    @JsonProperty("genus_name")
    private String genusName;
    @JsonProperty("scientific_name")
    private String scientificName;
    @JsonProperty("infra_rank")
    private Object infraRank;
    @JsonProperty("infra_name")
    private Object infraName;
    @JsonProperty("population")
    private Object population;
    @JsonProperty("category")
    private String category;

    @JsonProperty("title")
    private String title;

    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    @JsonProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }

    @JsonProperty("taxonid")
    public Long getTaxonid() {
        return taxonid;
    }

    @JsonProperty("taxonid")
    public void setTaxonid(Long taxonid) {
        this.taxonid = taxonid;
    }

    @JsonProperty("kingdom_name")
    public String getKingdomName() {
        return kingdomName;
    }

    @JsonProperty("kingdom_name")
    public void setKingdomName(String kingdomName) {
        this.kingdomName = kingdomName;
    }

    @JsonProperty("phylum_name")
    public String getPhylumName() {
        return phylumName;
    }

    @JsonProperty("phylum_name")
    public void setPhylumName(String phylumName) {
        this.phylumName = phylumName;
    }

    @JsonProperty("class_name")
    public String getClassName() {
        return className;
    }

    @JsonProperty("class_name")
    public void setClassName(String className) {
        this.className = className;
    }

    @JsonProperty("order_name")
    public String getOrderName() {
        return orderName;
    }

    @JsonProperty("order_name")
    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    @JsonProperty("family_name")
    public String getFamilyName() {
        return familyName;
    }

    @JsonProperty("family_name")
    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    @JsonProperty("genus_name")
    public String getGenusName() {
        return genusName;
    }

    @JsonProperty("genus_name")
    public void setGenusName(String genusName) {
        this.genusName = genusName;
    }

    @JsonProperty("scientific_name")
    public String getScientificName() {
        return scientificName;
    }

    @JsonProperty("scientific_name")
    public void setScientificName(String scientificName) {
        this.scientificName = scientificName;
    }

    @JsonProperty("infra_rank")
    public Object getInfraRank() {
        return infraRank;
    }

    @JsonProperty("infra_rank")
    public void setInfraRank(Object infraRank) {
        this.infraRank = infraRank;
    }

    @JsonProperty("infra_name")
    public Object getInfraName() {
        return infraName;
    }

    @JsonProperty("infra_name")
    public void setInfraName(Object infraName) {
        this.infraName = infraName;
    }

    @JsonProperty("population")
    public Object getPopulation() {
        return population;
    }

    @JsonProperty("population")
    public void setPopulation(Object population) {
        this.population = population;
    }

    @JsonProperty("category")
    public String getCategory() {
        return category;
    }

    @JsonProperty("category")
    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("taxonid", taxonid)
                .append("kingdomName", kingdomName)
                .append("phylumName", phylumName)
                .append("className", className)
                .append("orderName", orderName)
                .append("familyName", familyName)
                .append("genusName", genusName)
                .append("scientificName", scientificName)
                .append("infraRank", infraRank)
                .append("infraName", infraName)
                .append("population", population)
                .append("category", category)
                .append("title", title)
                .toString();
    }
}
