package at.mavila.iucnrlts.pojos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "name",
        "result"
})
public class Measures {

    @JsonProperty("name")
    private String name;
    @JsonProperty("result")
    private List<Measure> result = new ArrayList<>();

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(final String name) {
        this.name = name;
    }

    @JsonProperty("result")
    public List<Measure> getResult() {
        return result;
    }

    @JsonProperty("result")
    public void setResult(final List<Measure> result) {
        this.result = result;
    }
}
