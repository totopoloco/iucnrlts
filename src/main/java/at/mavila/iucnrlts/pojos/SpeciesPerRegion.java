package at.mavila.iucnrlts.pojos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "count",
        "region_identifier",
        "page",
        "result"
})
public class SpeciesPerRegion {
    @JsonProperty("count")
    private Integer count;
    @JsonProperty("region_identifier")
    private String regionIdentifier;
    @JsonProperty("page")
    private String page;
    @JsonProperty("result")
    private List<Specie> result = new ArrayList<>();

    @JsonProperty("count")
    public Integer getCount() {
        return count;
    }

    @JsonProperty("count")
    public void setCount(Integer count) {
        this.count = count;
    }

    @JsonProperty("region_identifier")
    public String getRegionIdentifier() {
        return regionIdentifier;
    }

    @JsonProperty("region_identifier")
    public void setRegionIdentifier(String regionIdentifier) {
        this.regionIdentifier = regionIdentifier;
    }

    @JsonProperty("page")
    public String getPage() {
        return page;
    }

    @JsonProperty("page")
    public void setPage(String page) {
        this.page = page;
    }

    @JsonProperty("result")
    public List<Specie> getResult() {
        return result;
    }

    @JsonProperty("result")
    public void setResult(List<Specie> result) {
        this.result = result;
    }
}
