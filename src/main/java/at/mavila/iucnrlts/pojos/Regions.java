package at.mavila.iucnrlts.pojos;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "count", "results"
})
public class Regions {

    @JsonProperty("count")
    private Integer count;

    @JsonProperty("results")
    private List<Region> regions = new ArrayList<>();

    @JsonProperty("count")
    public Integer getCount() {
        return this.count;
    }

    @JsonProperty("count")
    public void setCount(Integer count) {
        this.count = count;
    }

    @JsonProperty("results")
    public List<Region> getResults() {
        return regions;
    }

    @JsonProperty("results")
    public void setResults(final List<Region> regions) {
        this.regions = regions;
    }

}
