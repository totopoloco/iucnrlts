package at.mavila.iucnrlts;

import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;
import java.util.List;

public final class ConnectionUtils {

    private static final ThreadLocal<RestTemplate> REST_TEMPLATE = ThreadLocal.withInitial(RestTemplate::new);
    private static final StringHttpMessageConverter HTTP_MESSAGE_CONVERTER = new StringHttpMessageConverter(Charset.forName("UTF-8"));

    /**
     * No instances
     */
    private ConnectionUtils() {
        //Forbidden to create instances of this class.
    }

    public static RestTemplate getRestTemplate(){
        final RestTemplate restTemplate = REST_TEMPLATE.get();
        final List<HttpMessageConverter<?>> messageConverters = restTemplate.getMessageConverters();
        if(messageConverters.contains(HTTP_MESSAGE_CONVERTER)){
            return restTemplate;
        }
        restTemplate.getMessageConverters().add(0, HTTP_MESSAGE_CONVERTER);
        return restTemplate;
    }
}
